﻿namespace ASPNETCore.HowTos.Swashbuckle.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Values Controller V2
    /// </summary>
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/values")]
    public class Values2Controller : ControllerBase
    {

        /// <summary>
        /// GET Request V2
        /// </summary>
        /// <param name="apiVersion"></param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/values?api-version=2.0
        ///
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public string Get(ApiVersion apiVersion) =>
            $"Controller = {this.GetType().Name}\nVersion = {apiVersion}";
    }
}
